filterSectionAll = document.querySelectorAll('.filter-section li');
filterSection = document.querySelector('.filter-section');



//add active
filterSection.addEventListener('click', function(){
    
    for (let i = 0; i < filterSectionAll.length; i++) {
        const filterSectionLi = filterSectionAll[i];
        

        filterSectionLi.addEventListener('click', function(e){        
            this.classList.add('active');
        })
    }   
})

//add image in grid
var filteredListItem = document.getElementsByClassName('filtered-list__item');

for (var i = 0; i < filteredListItem.length; i++) {
  var src = filteredListItem[i].getAttribute('data-image-src');
  filteredListItem[i].style.backgroundImage="url('" + src + "')";
}
//add image in popup
var popupWindowAvatar = document.getElementsByClassName('popup__window__avatar');

for (var i = 0; i < popupWindowAvatar.length; i++) {
  var src = popupWindowAvatar[i].getAttribute('data-image-src');
  popupWindowAvatar[i].style.backgroundImage="url('" + src + "')";
}